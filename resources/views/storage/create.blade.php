@extends('layouts.app');
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header"><h1>Storage create</h1></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('storage.store') }}" method="post">
                    {{ csrf_field() }}
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @include('storage.partials._form')
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop