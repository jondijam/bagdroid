<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    protected $fillable = ["departure", "arrival"];
    protected $dates = ["created_at", "updated_at", "departure", "arrival"];

    public function scopeByTeam($query, $team)
    {
        return $query->whereHas('flight_route', function($q) use($team)
        {
            $q->whereHas('airline', function ($qq) use($team)
            {
                $qq->whereHas('teams', function ($qqq) use($team)
                {
                    $qqq->where('id', $team->id);
                });
            });
        });
    }

    public function flight_route()
    {
        return $this->belongsTo(FlightRoute::class);
    }

    public function aircraft()
    {
        return $this->belongsTo(Aircraft::class);
    }

    public function multiple_unit_storage()
    {
        return $this->hasMany(UnitStorage::class);
    }

    public function units()
    {
        return $this->belongsToMany(Unit::class);
    }

    public function assignAircraft(Aircraft $aircraft)
    {
        $this->aircraft()->associate($aircraft)->save();
    }
}
