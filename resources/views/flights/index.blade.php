@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>Flights</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Flight Number</th>
                                    <th>Origin</th>
                                    <th>Destination</th>
                                    <th>Departure time</th>
                                    <th>Arrival time</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($flights as $flight)
                                    <tr>
                                        <td>
                                            {{ $flight->flight_route->flightNumber() }}
                                        </td>
                                        <td>
                                            {{ $flight->flight_route->origin }}
                                        </td>
                                        <td>
                                            {{ $flight->flight_route->destination }}
                                        </td>
                                        <td>
                                            {{ $flight->departure }}
                                        </td>
                                        <td>
                                            {{ $flight->arrival }}
                                        </td>
                                        <td>
                                            {{ $flight->status }}
                                        </td>
                                        <td>
                                            <a href="{{ route('flights.show', ['flights'=>$flight->id]) }}">View</a>
                                            <a href="{{ route('flights.edit', ['flights'=>$flight->id]) }}">Edit</a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="{{ route('flights.create') }}" class="btn btn-default">Create new flight</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop