<?php
/**
 * Created by PhpStorm.
 * User: jondijam
 * Date: 19.9.2016
 * Time: 10:45
 */

namespace App\Bagdroid\Repo\Airline;

use Illuminate\Contracts\Cache\Repository as Cache;



class CacheAirlineRepository implements AirlineRepository
{
    protected $airline;
    protected $cache;
    public function __construct(AirlineRepository $airline, Cache $cache)
    {
        $this->airline = $airline;
        $this->cache = $cache;
    }

    public function getAll()
    {
        $cacheKey = "all";
        return $this->cache->remember($cacheKey, 30, function ()
        {
            return $this->airline->getAll();
        });
    }

    public function getById($id, array $with = [])
    {
        $cacheKey = "airline-id.".$id;
        return $this->cache->remember($cacheKey, 30, function () use($id, $with)
        {
            return $this->airline->getById($id, $with);
        });

        // TODO: Implement getById() method.
    }

    public function getFirstBy($key, $value, array $with = array())
    {
        $cacheKey = "airline-first.".$key.".".$value;
        // TODO: Implement getFirstBy() method.
        return $this->cache->remember($cacheKey, 30, function () use($key, $value, $with)
        {
            return $this->airline->getFirstBy($key, $value, $with);
        });


    }

    public function getManyBy($key, $value, array $with = array())
    {
        $cacheKey = "airline-many.".$key.".".$value;

        return $this->cache->remember($cacheKey, 30, function () use($key, $value, $with)
        {
            return $this->airline->getManyBy($key, $value, $with);
        });
        // TODO: Implement getManyBy() method.
    }

    public function getByPage($page = 1, $limit = 10, $with = array())
    {
        $cacheKey = "airline-page.".$page.'.'.$limit;

        return $this->cache->remember($cacheKey, 30, function () use($page, $limit, $with)
        {
            return $this->airline->getByPage($page, $limit, $with);
        });

        // TODO: Implement getByPage() method.
    }

}