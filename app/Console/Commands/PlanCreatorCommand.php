<?php

namespace App\Console\Commands;

use Braintree\Plan;
use Illuminate\Console\Command;

class PlanCreatorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plan:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Save plans from braintree payment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $braintreePlans = Plan::all();

        foreach ($braintreePlans as $braintreePlan)
        {
            if($braintreePlan->id === 1)
            {
                \App\Plan::create([
                    "braintree_id"=>$braintreePlan->id,
                    'braintree_plan'=>$braintreePlan->name,
                    'price'=>$braintreePlan->price
                ]);
            }
        }
    }
}
