@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="page-header">
                <h1>Flight: {{ $flight->flight_route->flightNumber() }} {{ $flight->departure->format("d.m.Y") }}</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h2>Aircraft <small>{{ $flight->aircraft->registration_number or null }}</small></h2>
                <div class="panel panel-default">
                    <div class="panel-body">
                        @if($flight->aircraft)
                        @else
                            <form action="{{ route('flights.aircraft.store', ['flights'=>$flight->id]) }}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Assign aircraft</label>
                                    <select name="aircraft_id" class="form-control">
                                        @foreach($flight->flight_route->airline->aircraft as $aircraft )
                                            <option value="{{ $aircraft->id }}">{{ $aircraft->registration_number }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="submit">Assign aircraft to flight</button>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($categories as $category)
                <div class="col-md-3">
                    <h3>{{ $category->name }}</h3>
                    @foreach($category->unitStorageByFlight($flight->id) as $storage)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">{{ $storage->unit->name }} {{ $storage->number }}</h4>
                            </div>
                            <div class="panel-body">
                                @if($storage->descendants()->count() > 0)
                                    @foreach($storage->descendants as $storage_child)
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">{{ $storage_child->unit->name }} {{ $storage_child->number }}</h4>
                                            </div>
                                            <div class="panel-body">
                                                <ul>
                                                    @foreach($storage_child->bags as $bag)
                                                        <li>{{ $bag->tag_number }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <ul>
                                        @foreach($storage->bags as $bag)
                                            <li>{{ $bag->tag_number }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
@stop