@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>Flight Routes</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Flight number</th>
                                    <th>Origin</th>
                                    <th>Destination</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($flightRoutes as $flightRoute)
                                    <tr>
                                        <td>
                                            {{ $flightRoute->flightNumber() }}
                                        </td>
                                        <td>
                                            {{ $flightRoute->origin }}
                                        </td>
                                        <td>
                                            {{ $flightRoute->destination }}
                                        </td>
                                        <td></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="{{ route('routes.create') }}" class="btn btn-default">Create</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop