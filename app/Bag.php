<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Bag extends Model
{
    public function unitStorage()
    {
        return $this->morphToMany('App\UnitStorage', 'storage','storage');
    }
}
