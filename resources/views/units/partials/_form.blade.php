<div class="form-group form-group-lg">
    <label>Name</label>
    <input type="text" name="name" value="{{ $unit->name or old('name') }}" class="form-control" />
</div>
<div class="checkbox">
    <label><input type="checkbox" @if($unit->for_aircraft) checked="checked" @endif name="for_aircraft" value="true" /> For aircraft</label>
</div>