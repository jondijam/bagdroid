<?php
/**
 * Created by PhpStorm.
 * User: jondijam
 * Date: 19.8.2016
 * Time: 07:29
 */

namespace App\Http\Composers;

use Illuminate\View\View;

class AppComposer
{
    public function compose(View $view)
    {
        $currentTeam = null;

        if(\Auth::check())
        {
            $currentTeam = \Auth::user()->currentTeam or null;
        }

        $view->with('currentTeam', $currentTeam);
    }
}