@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>Edit an unit load device</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('unit_load_devices.update', ['unit_load_devices'=>$unitLoadDevice->id]) }}"
                      method="post"
                >
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">Unit load device</h2>
                        </div>
                        <div class="panel-body">
                            @include('unit_load_devices.partials._form')
                        </div>
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Edit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop