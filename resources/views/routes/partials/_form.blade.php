<div class="checkbox">
    <label><input type="checkbox" name="is_recurring" value="true"/> Register flight automatically for the route?</label>
</div>
<div class="form-group">
    <label>Airline</label>
    <select name="airline_id" class="form-control">
        @foreach($airlines as $airline)
            <option value="{{ $airline->id }}">
                {{ $airline->name }}
            </option>
        @endforeach
    </select>
</div>
<div class="form-group @if($errors->has('flight_number')) has-error @endif">
    <label>Flight number</label>
    <input type="text" name="flight_number" class="form-control" value="{{ old('origin') }}" />

</div>
<div class="form-group @if($errors->has('origin')) has-error @endif">
    <label>Origin</label>
    <input type="text" name="origin" class="form-control" value="{{ $origin }}" />
    {{-- <input type="hidden" name="origin" value="{{ $origin }}" />--}}
</div>
<div class="form-group @if($errors->has('destination')) has-error @endif" >
    <label>Destination</label>
    <input type="text" name="destination" class="form-control" value="{{ old('destination') }}" />
</div>
