<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Airline extends Model
{
    protected $fillable = ['name','code'];
    public function teams()
    {
        return $this->belongsToMany(Team::class);
    }

    public function aircraft()
    {
        return $this->hasMany(Aircraft::class);
    }

    public function flightRoutes()
    {
        return $this->hasMany(FlightRoute::class);
    }
}
