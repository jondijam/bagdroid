<?php
/**
 * Created by PhpStorm.
 * User: jondijam
 * Date: 8.10.2016
 * Time: 21:58
 */

namespace App\Bagdroid\Repo\UnitLoadDevice;

use App\Bagdroid\Repo\DatabaseRepository;
use App\UnitLoadDevice;
use stdClass;

class EloquentUnitLoadDevice extends DatabaseRepository implements UnitLoadDeviceContract
{
    protected $model;
    public function __construct(UnitLoadDevice $model)
    {
        parent::__construct($model);
    }
}