<?php
/**
 * Created by PhpStorm.
 * User: jondijam
 * Date: 19.9.2016
 * Time: 10:36
 */

namespace App\Bagdroid\Repo\Airline;

use App\Airline;
use App\Bagdroid\Repo\DatabaseRepository;

class EloquentAirlineRepository extends DatabaseRepository implements AirlineRepository
{
    protected $model;
    public function __construct(Airline $model)
    {
        $this->model = $model;
        parent::__construct($model);
    }

}