<div class="form-group">
    <label>Flight</label>
    <select></select>
</div>
<div class="form-group">
    <label>Unit</label>
    <select class="form-control" name="unit_id">
        @foreach($units as $unit)
            <option value="{{ $unit->id }}">{{ $unit->name }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label></label>
</div>