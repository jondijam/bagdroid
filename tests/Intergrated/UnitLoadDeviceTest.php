<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UnitLoadDeviceTest extends TestCase
{
    /**
     * A basic test example.
     * @test
     */
    public function a_user_visit_index_page_of_unit_load_devices()
    {
        $user = \App\User::with([])->first();
        $this->actingAs($user)->visit('/unit_load_devices');
    }
}
