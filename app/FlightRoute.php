<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlightRoute extends Model
{
    protected $fillable = ["origin", "destination", "flight_number", "is_recurring"];

    public function flightNumber()
    {
        return $this->airline->code.$this->flight_number;
    }

    public function flights()
    {
        return $this->hasMany(Flight::class);
    }

    public function airline()
    {
        return $this->belongsTo(Airline::class);
    }
}
