<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\User;

class UserTest extends TestCase
{
    public function he_visit_profile()
    {

    }

    /**
     * @test
    */
    public function has_a_team()
    {
        $user = User::with('currentTeam')->find(1);
        $this->assertEquals('team', $user->currentTeam->name);
    }
}
