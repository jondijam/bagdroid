<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = ['name'];

    public function scopeForAircraft($query)
    {
        return $query->where('for_aircraft', '=', true);
    }


    public function unitStorage()
    {
        return $this->hasMany(UnitStorage::class);
    }

}
