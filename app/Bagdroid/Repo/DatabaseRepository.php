<?php
/**
 * Created by PhpStorm.
 * User: jondijam
 * Date: 19.9.2016
 * Time: 10:37
 */

namespace App\Bagdroid\Repo;

use stdClass;

abstract class DatabaseRepository
{
    protected $model;
    public function __construct($model)
    {
        $this->model = $model;
    }

    protected function make(array $with = [])
    {
        return $this->model->with($with);
    }

    /**
     * Get all items of unit load devices
     */
    public function getAll()
    {
        return $this->model->get();
    }

    public function getById($id, array $with = [])
    {
        return $this->make($with)->find($id);
    }

    /**
     * Find a single entity by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getFirstBy($key, $value, array $with = array())
    {
        return $this->make($with)->where($key, '=', $value)->first();
    }

    /**
     * Find many entities by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getManyBy($key, $value, array $with = array())
    {
        return $this->make($with)->where($key, '=', $value)->get();
    }

    /**
     * Get Results by Page
     *
     * @param int $page
     * @param int $limit
     * @param array $with
     * @return \StdClass Object with $items and $totalItems for pagination
     */
    public function getByPage($page = 1, $limit = 10, $with = array())
    {
        $result             = new stdClass;
        $result->page       = $page;
        $result->limit      = $limit;
        $result->totalItems = 0;
        $result->items      = array();

        $query = $this->make($with);

        $model = $query->skip($limit * ($page - 1))
            ->take($limit)
            ->get();

        $result->totalItems = $this->model->count();
        $result->items      = $model->all();

        return $result;
    }
}