<?php

namespace App\Listeners;

use App\Events\UserWasCreated;
use App\Role;
use App\Team;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

class CreateTeam
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserWasCreated  $event
     * @return void
     */
    public function handle(UserWasCreated $event)
    {
        $role = Role::with([])->role('owner')->first();

        $event->data['trial_ends_at'] = Carbon::now()->addDays(30);

        $team = new Team($event->data);
        $team->airport()->associate($event->data['airport_id']);
        $team->owner()->associate($event->user);
        $team->save();

        $event->user->currentTeam()->associate($team);
        $event->user->save();
        $event->user->teams()->attach($team->id, ['role_id'=>$role->id]);
    }
}
