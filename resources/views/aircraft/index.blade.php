@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>Aircraft</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel-default panel">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Airline</th>
                                    <th>Type</th>
                                    <th>Registration number</th>
                                    <th>Storage quantity</th>
                                    <th>Type of unit</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($airlines as $airline)
                                @foreach($airline->aircraft as $aircraft)
                                    <tr>
                                        <td>
                                            {{ $airline->name }}
                                        </td>
                                        <td>
                                            {{ $aircraft->type }}
                                        </td>
                                        <td>
                                            {{ $aircraft->registration_number }}
                                        </td>
                                        <td>
                                            {{ $aircraft->storage_quantity }}
                                        </td>
                                        <td>
                                            {{ $aircraft->unit->name }}
                                        </td>
                                    </tr>
                                @endforeach
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-body">
                         <a href="{{ route('aircraft.create') }}" class="btn btn-primary">Create</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop