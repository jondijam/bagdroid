<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mpociot\Teamwork\Traits\UserHasTeams;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Config;

class User extends Authenticatable
{
    use Notifiable, UserHasTeams, EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function IsMemberOfCurrentTeam()
    {
        return true;
    }

    public function ownedTeams()
    {
        return $this->hasOne(Team::class, 'owner_id');
    }

    public function currentTeam()
    {
        return $this->belongsTo(Config::get( 'teamwork.team_model'), 'team_id');
    }

    public function newTeam($attributes = [], $roleId)
    {
        $team = new Team($attributes);
        $team->owner()->associate($this);
        $team->usersWithRoles()->attach($this->id, ['role'=>$roleId]);
        $team->save();
    }

    public function teams()
    {
        return $this->belongsToMany(Config::get('teamwork.team_model'), Config::get('teamwork.team_user_table'), 'user_id','team_id')->withPivot('role_id')->withTimestamps();
    }

    public function rolesWithTeams()
    {
        return $this->belongsToMany(Config::get('teamwork.role_model'), Config::get('teamwork.team_user_table'), 'user_id','role_id')->withPivot('team_id')->withTimestamps();
    }
}
