<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class UnitStorage extends Model
{
    use NodeTrait;
    protected $fillable = ["code", "number", "quantity"];
    protected $table = "unit_storage";

    public function flight()
    {
        return $this->belongsTo(Flight::class);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function bags()
    {
        return $this->morphedByMany('App\Bag', 'storage','storage');
    }
}
