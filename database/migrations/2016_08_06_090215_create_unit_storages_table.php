<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitStoragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_storage', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('number');
            $table->integer('quantity')->default(0);
            $table->integer('unit_id');
            $table->integer('flight_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('unit_storage');
    }
}
