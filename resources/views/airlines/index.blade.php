@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>Airlines</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>IATA Code</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($airlines->items() as $airline)
                                <tr>
                                    <td>{{ $airline->name }}</td>
                                    <td> {{ $airline->code }}</td>
                                    <td>
                                        <a href="{{ route('airlines.edit', ['airlines'=>$airline->id]) }}" class="btn btn-default">Edit</a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="{{ route('airlines.create') }}" class="btn btn-primary">Create</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop