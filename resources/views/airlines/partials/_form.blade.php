<div class="form-group">
    <label>Name</label>
    <input type="text" name="name" class="form-control" value="{{ $airline->name or old('name') }}" />
</div>
<div class="form-group">
    <label>Code</label>
    <input type="text" name="code" class="form-control" value="{{ $airline->code or old('code')}}" />
</div>