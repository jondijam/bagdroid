<div class="form-group">
    <label>Flight Routes</label>
    <select class="form-control" name="flight_route_id">
        @foreach($airlines as $airline)
            <optgroup label="{{ $airline->name }}">
                @foreach($airline->flightRoutes as $flightRoute)
                    <option value="{{ $flightRoute->id }}">{{ $airline->code }} {{ $flightRoute->flight_number }} {{ $flightRoute->origin }} - {{ $flightRoute->destination }}</option>
                @endforeach
            </optgroup>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label>Departure</label>
    <div class='input-group date' id='departure'>
        <input type="text" name="departure" class="form-control" />
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>

</div>
<div class="form-group">
    <label>Arrival</label>
    <div class='input-group date' id='arrival'>
        <input type="text" name="arrival" class="form-control" />
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
</div>
<div>

</div>