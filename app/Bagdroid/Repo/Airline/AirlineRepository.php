<?php
/**
 * Created by PhpStorm.
 * User: jondijam
 * Date: 19.9.2016
 * Time: 10:45
 */
namespace App\Bagdroid\Repo\Airline;



interface AirlineRepository
{
    public function getById($id, array $with = []);

    public function getAll();

    /**
     * Find a single entity by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getFirstBy($key, $value, array $with = array());

    /**
     * Find many entities by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getManyBy($key, $value, array $with = array());

    /**
     * Get Results by Page
     *
     * @param int $page
     * @param int $limit
     * @param array $with
     * @return StdClass Object with $items and $totalItems for pagination
     */
    public function getByPage($page = 1, $limit = 10, $with = array());
}