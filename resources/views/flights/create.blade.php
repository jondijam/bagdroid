@extends('layouts.app')
@section('content')
    <div class="container" id="flight-editor">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>Create a new flight</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form action="{{ route('flights.store') }}" method="post">
                    {{ csrf_field() }}
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @include('flights.partials._form')
                        </div>
                        <div class="panel-footer">
                            <button type="submit">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('scripts.footer')
    <script src="{{ asset("js/flight.js") }}"></script>
@stop