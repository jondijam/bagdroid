<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', ['as'=>"home.index", "uses"=>"HomeController@index"]);
Route::group(['middleware'=>["auth"], 'namespace'=>"Web"], function()
{
    Route::resource('teams', 'TeamsController');
    Route::resource('flights', 'FlightsController');
    Route::resource('routes', 'RoutesController');
    Route::resource('units', 'UnitsController');
    Route::resource('storage', 'StorageController');

    Route::group(["prefix"=>"settings", 'namespace'=>"settings"], function ()
    {
        Route::resource('teams', 'TeamsController');
        Route::resource('teams.invitation', 'TeamInvitationController');
        Route::resource('teams.subscription','TeamSubscriptionController');
        Route::resource('payments_methods', 'PaymentMethodsController');
        Route::resource('users', 'UsersController');
    });

});




Auth::routes();

Route::get('/home', 'HomeController@index');
