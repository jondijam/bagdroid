<?php
/**
 * Created by PhpStorm.
 * User: jondijam
 * Date: 20.9.2016
 * Time: 17:31
 */

namespace App\Bagdroid\Repo\Unit;


use App\Bagdroid\Repo\DatabaseRepository;
use App\Unit;

class EloquentUnitRepository extends DatabaseRepository implements UnitRepository
{
    protected $model;
    public function __construct(Unit $model)
    {
        parent::__construct($model);
    }
}