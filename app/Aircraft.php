<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aircraft extends Model
{
    protected $fillable = ['registration_number','storage_quantity','type','storage_type'];
    public function airline()
    {
        return $this->belongsTo(Airline::class);
    }

    public function flights()
    {
        return $this->hasMany(Flight::class);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }
}
