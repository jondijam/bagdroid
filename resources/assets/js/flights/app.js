var app = new Vue({
    el:"#flight-editor",
    ready:function () {
        $('#departure').datetimepicker({
            format:"YYYY-MM-DD H:mm:SS"
        });

        $('#arrival').datetimepicker({
            format:"YYYY-MM-DD H:mm:SS"
        });
    }
});