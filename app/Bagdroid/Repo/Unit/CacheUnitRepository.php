<?php
/**
 * Created by PhpStorm.
 * User: jondijam
 * Date: 20.9.2016
 * Time: 17:33
 */

namespace App\Bagdroid\Repo\Unit;

use Illuminate\Contracts\Cache\Repository as Cache;


class CacheUnitRepository implements UnitRepository
{
    protected $unit;
    protected $cache;
    public function __construct(UnitRepository $unit, Cache $cache)
    {
        $this->unit = $unit;
        $this->cache = $cache;
    }

    public function getAll()
    {
        $cacheKey = "unit.all";
        return $this->cache->remember($cacheKey, 30, function ()
        {
            return $this->unit->getAll();
        });
        // TODO: Implement getAll() method.
    }

    public function getById($id, array $with = [])
    {
        $cacheKey = "unit-id.".$id;
        return $this->cache->remember($cacheKey, 30, function () use($id, $with)
        {
            return $this->unit->getById($id, $with);
        });

        // TODO: Implement getById() method.
    }

    /**
     * Find a single entity by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     * @return mixed
     */
    public function getFirstBy($key, $value, array $with = array())
    {
        $cacheKey = "unit-first.".$key.".".$value;
        // TODO: Implement getFirstBy() method.
        return $this->cache->remember($cacheKey, 30, function () use($key, $value, $with)
        {
            return $this->unit->getFirstBy($key, $value, $with);
        });
        // TODO: Implement getFirstBy() method.
    }

    /**
     * Find many entities by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     * @return mixed
     */
    public function getManyBy($key, $value, array $with = array())
    {
        $cacheKey = "unit-many.".$key.".".$value;

        return $this->cache->remember($cacheKey, 30, function () use($key, $value, $with)
        {
            return $this->unit->getManyBy($key, $value, $with);
        });
        // TODO: Implement getManyBy() method.
    }

    /**
     * Get Results by Page
     *
     * @param int $page
     * @param int $limit
     * @param array $with
     * @return mixed
     */
    public function getByPage($page = 1, $limit = 10, $with = array())
    {
        $cacheKey = "unit-page.".$page.'.'.$limit;

        return $this->cache->remember($cacheKey, 30, function () use($page, $limit, $with)
        {
            return $this->unit->getByPage($page, $limit, $with);
        });
    }
}