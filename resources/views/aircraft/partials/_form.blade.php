<div class="form-group">
    <label>Airline</label>
    <select class="form-control" name="airline_id">
        @foreach($airlines as $airline)
            <option value="{{ $airline->id }}">{{ $airline->name }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label>Type</label>
    <input type="text" name="type" class="form-control" value="" />
</div>
<div class="form-group">
    <label>Registration number</label>
    <input type="text" name="registration_number" class="form-control" value="" />
</div>
<div class="form-group">
    <label>Storage quantity</label>
    <input type="text" name="storage_quantity" class="form-control" value="" />
</div>
<div class="form-group">
    <label>Type of unit</label>
    <select name="unit_id" class="form-control">
        @foreach($units as $unit)
            <option value="{{ $unit->id }}">{{ $unit->name }}</option>
        @endforeach
    </select>
</div>