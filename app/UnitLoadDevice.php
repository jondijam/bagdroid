<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class UnitLoadDevice extends Model
{
    protected $fillable = ["type", "serial_code", "barcode"];

    public function setSerialCodeAttribute($value)
    {
        if($value === "0")
        {
            $this->attributes['serial_code'] = null;
        }
        else
        {
            $this->attributes['serial_code'] = $value;
        }

    }

    public function getSerialCodeAttribute($value)
    {
        if($value == null);
        {
            $this->attributes['serial_code'] = 0;
        }
    }
}
