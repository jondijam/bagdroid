<?php

namespace App\Http\Controllers\Web;

use App\Airline;
use App\Team;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AirlinesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::user();
        $team =  $user->currentTeam()->with('airlines')->first();
        $airlines = $team->airlines()->paginate(10);
        return view('airlines.index', compact('airlines', compact('airlines')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('airlines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name'=>"required", "code"=>"required"]);

        $code = $request->get('code');
        $airline = Airline::where('code', $code)->first();

        if($airline === null)
        {
            $airline = new Airline($request->all());
            $airline->save();
        }

        $team = \Auth::user()->currentTeam()->first();
        $airline->teams()->attach($team->id);

        return redirect()->route('airlines.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  Airline  $airline
     * @return \Illuminate\Http\Response
     */
    public function show(Airline $airline)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Airline  $airline
     * @return \Illuminate\Http\Response
     */
    public function edit(Airline $airline)
    {
        return view('airlines.edit', compact('airline'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Airline $airline
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Airline $airline)
    {
        $this->validate($request, ['name'=>"required", "code"=>"required"]);
        $airline->update($request->all());

        return redirect()->route('airlines.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Airline $airline
     * @return \Illuminate\Http\Response
     */
    public function destroy(Airline $airline)
    {
        $team = \Auth::user()->currentTeam()->first();
        $airline->teams()->detach($team->id);

        return redirect()->route('airlines.index');
    }
}
