<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;
use Carbon\Carbon;
use Mpociot\Teamwork\TeamworkTeam;

class Team extends TeamworkTeam
{
    protected $fillable = [
        "braintree_id",
        "card_brand",
        "card_last_four",
        "name",
        "paypal_email",
        "trial_ends_at"
    ];

    protected $dates = ["created_at", "updated_at", "trial_ends_at"];

    use Billable;

    public function airport()
    {
        return $this->belongsTo(Airport::class);
    }

    public function airlines()
    {
        return $this->belongsToMany(Airline::class);
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function current()
    {
        return $this->hasOne(User::class, 'team_id');
    }

    public function rolesWithUsers()
    {
        return $this->belongsToMany(Config::get('teamwork.user_model'), Config::get('teamwork.team_user_table'), 'team_id','role_id')->withPivot('user_id')->withTimestamps();
    }

    /**
     * Many-to-Many relations with the user model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function usersWithRoles()
    {
        return $this->belongsToMany(Config::get('teamwork.user_model'), Config::get('teamwork.team_user_table'), 'team_id','user_id')->withPivot('role_id')->withTimestamps();
    }
}
