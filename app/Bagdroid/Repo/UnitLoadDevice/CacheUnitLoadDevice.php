<?php
/**
 * Created by PhpStorm.
 * User: jondijam
 * Date: 8.10.2016
 * Time: 22:03
 */

namespace App\Bagdroid\Repo\UnitLoadDevice;


class CacheUnitLoadDevice implements UnitLoadDeviceContract
{
    protected $unitLoadDevice;
    public function __construct(UnitLoadDeviceContract $unitLoadDevice)
    {
        $this->unitLoadDevice = $unitLoadDevice;
    }

    /**
     * Get all items of unit load devices
     */
    public function getAll()
    {
        $key = "unitLoadDevices.all";
        return \Cache::remember($key,60, function ()
        {
            return $this->unitLoadDevice->getAll();
        });

        // TODO: Implement getAll() method.
    }

    public function getById($id, array $with = [])
    {
        $key = "unitLoadDevices.id".$id;

        return \Cache::remember($key, 60, function () use ($id, $with)
        {
            return $this->unitLoadDevice->getById($id, $with);
        });

        // TODO: Implement getById() method.
    }

    /**
     * Find a single entity by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getFirstBy($key, $value, array $with = array())
    {
        $cacheKey = "unitLoadDevices.first.".$key.".".$value;
        // TODO: Implement getFirstBy() method.
        return \Cache::remember($cacheKey, 60, function () use($key, $value, $with)
        {
            return $this->unitLoadDevice->getFirstBy($key, $value, $with);
        });
    }

    /**
     * Find many entities by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getManyBy($key, $value, array $with = array())
    {
        $cacheKey = "unitLoadDevices.many.".$key.".".$value;
        return \Cache::remember($cacheKey, 60, function () use($key, $value, $with)
        {
            return $this->unitLoadDevice->getManyBy($key, $value, $with);
        });
        // TODO: Implement getManyBy() method.
    }

    /**
     * Get Results by Page
     *
     * @param int $page
     * @param int $limit
     * @param array $with
     * @return StdClass Object with $items and $totalItems for pagination
     */
    public function getByPage($page = 1, $limit = 10, $with = array())
    {
        $key = "unitLoadDevices.page.".$page.".".$limit;

        return \Cache::remember($key, 60, function ()use($page, $limit, $with)
        {
            return $this->unitLoadDevice->getByPage($page, $limit, $with);
        });
        // TODO: Implement getByPage() method.
    }
}