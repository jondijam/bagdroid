<?php
/**
 * Created by PhpStorm.
 * User: jondijam
 * Date: 8.10.2016
 * Time: 22:00
 */
namespace App\Bagdroid\Repo\UnitLoadDevice;

interface UnitLoadDeviceContract
{
    /**
     * Get all items of unit load devices
    */
    public function getAll();

    public function getById($id, array $with = []);

    /**
     * Find a single entity by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getFirstBy($key, $value, array $with = array());

    /**
     * Find many entities by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getManyBy($key, $value, array $with = array());

    /**
     * Get Results by Page
     *
     * @param int $page
     * @param int $limit
     * @param array $with
     */
    public function getByPage($page = 1, $limit = 10, $with = array());
}