<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'slug'];

    public function unitStorage()
    {
        return $this->hasMany(UnitStorage::class);
    }

    public function unitStorageByFlight($flightId)
    {
        return $this->hasMany(UnitStorage::class)->whereHas('flight', function ($q) use($flightId)
        {
            $q->where('id', $flightId);
        })->get();
    }
}
