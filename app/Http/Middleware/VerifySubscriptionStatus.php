<?php

namespace App\Http\Middleware;

use App\Plan;
use Closure;

class VerifySubscriptionStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $plans = Plan::all();

        foreach ($plans as $plan)
        {
            if ($request->user() && (!$request->user()->currentTeam->subscribed($plan->braintree_plan) || !$request->user()->currentTeam->onTrial()))
            {
                $currentTeamId = $request->user()->currentTeam->id;
                // This user is not a paying customer...
                return redirect()->route('teams.subscription', ['teams'=>$currentTeamId]);
            }
        }

        return $next($request);
    }
}
