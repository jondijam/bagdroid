@extends('layouts.app')
@section('content')
    <div class="container" id="subscription" client-key="{{ \Braintree\ClientToken::generate() }}">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>Subscription</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-sm-10">
            <form action="{{ route('settings.teams.subscription.store', compact('team')  ) }}"
                  method="POST"
                  id="payment-form"
            >
                {{ csrf_field() }}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">Plans</h2>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($plans as $plan)
                                    <tr>
                                        <td>{{ $plan->braintree_plan }}</td>
                                        <td>${{ $plan->price }} per month</td>
                                        <td>
                                            <button type="button" class="btn btn-primary btn-success" :click="selectPlan({{ $plan->id }})">Selected</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">Payment method</h2>
                    </div>
                    <div class="panel-body">
                        <div>
                            <div class="form-group">
                                <label>First name</label>
                                <input type="text" name="first_name" class="form-control" value="{{ Auth::user()->first_name }}" />
                            </div>
                            <div class="form-group">
                                <label>Last name</label>
                                <input type="text" name="last_name" class="form-control" value="{{ Auth::user()->last_name }}" />
                            </div>
                            <div class="form-group">
                                <label>Company</label>
                                <input type="text" name="company" class="form-control" value="{{ $team->name }}" />
                            </div>
                            <div class="form-group">
                                <label for="card-number">Card Number</label>
                                <div class="hosted-field form-control" id="card-number"></div>
                            </div>
                            <div class="form-group">
                                <label for="cvv">CVV</label>
                                <div class="hosted-field form-control" id="cvv"></div>
                            </div>
                            <div class="form-group">
                                <label for="expiration-date">Expiration Date</label>
                                <div class="hosted-field form-control"  id="expiration-date"></div>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="payment-method-nonce">
                                <input type="hidden" name="plan" value="@{{ plan_selected }}" />
                                <input type="submit" value="Pay" :disabled="submit_disable" />
                            </div>
                            <pre>
                                @{{ $data | json }}
                            </pre>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
@stop
@section('scripts.footer')
    <script src="{{ elixir('js/subscription.js') }}"></script>
@stop