<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNestedSetToUnitStorage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unit_storage', function (Blueprint $table) {
            \Kalnoy\Nestedset\NestedSet::columns($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unit_storage', function (Blueprint $table) {
            \Kalnoy\Nestedset\NestedSet::dropColumns($table);
        });
    }
}
