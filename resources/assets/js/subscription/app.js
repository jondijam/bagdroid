import braintree from 'braintree-web';
var app = new Vue({
    el: '#subscription',
    props:['clientKey'],
    data:{
        plan_selected: 0,
        submit_disable:true,
        hostedFields:null
    },
    created:function () {

    },
    ready:function ()
    {
        var vm = this;
        var form = document.querySelector('#payment-form');
        var submit = document.querySelector('input[type="submit"]');
        braintree.client.create({
            authorization:  vm.clientKey
        }, function (clientErr, clientInstance) {
            if (clientErr) {
                console.error(clientErr);
                return;
            }

            // This example shows Hosted Fields, but you can also use this
            // client instance to create additional components here, such as
            // PayPal or Data Collector.

            braintree.hostedFields.create({
                client: clientInstance,
                styles: {
                    'input': {
                        'font-size': '14px'
                    },
                    'input.invalid': {
                        'color': 'red'
                    },
                    'input.valid': {
                        'color': 'green'
                    }
                },
                fields: {
                    number: {
                        selector: '#card-number',
                        placeholder: '4111 1111 1111 1111'
                    },
                    cvv: {
                        selector: '#cvv',
                        placeholder: '123'
                    },
                    expirationDate: {
                        selector: '#expiration-date',
                        placeholder: '10 / 2019'
                    }
                }
            }, function (hostedFieldsErr, hostedFieldsInstance) {
                if (hostedFieldsErr) {
                    console.error(hostedFieldsErr);
                    return;
                }

                vm.submit_disable = false;
                vm.hostedFields = hostedFieldsInstance;

                form.addEventListener('submit', function (event) {
                    event.preventDefault();

                    hostedFieldsInstance.tokenize(function (tokenizeErr, payload) {
                        if (tokenizeErr) {
                            console.error(tokenizeErr);
                            return;
                        }

                        // If this was a real integration, this is where you would
                        // send the nonce to your server.
                        document.querySelector('input[name="payment-method-nonce"]').value = payload.nonce;
                        form.submit();
                    });
                }, false);
            });
        });
    },
    methods:{
        selectPlan:require('./methods/selectPlan')
    }
});
