<div class="form-group">
    <label>Serial Code</label>
    <input type="text"
           class="form-control"
           name="serial_code"
           value="{{ $unitLoadDevice->serial_code or old('serial_code')}}" Required />
</div>
<div class="form-group">
    <label>Type</label>
    <select class="form-control"
            name="type"
    >
        <option value="pallet"
                @if($unitLoadDevice->type === "pallet") selected="selected" @endif
        >Pallet</option>
        <option value="container"
                @if($unitLoadDevice->type === "container" ) selected="selected" @endif
        >Container</option>
    </select>
</div>
<div class="form-group">
    <label>Barcode</label>
    <input type="hidden" name="barcode" value="{{ $code }}" />
    {!! $barcode  !!}
    <div style="position:relative;width:290px;height:30px;">
        <p class="text-center">{{ $code }}</p>
    </div>
</div>