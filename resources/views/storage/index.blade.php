@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>Storage</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Code</th>
                                    <th>Number</th>
                                    <th>Quantity</th>
                                    <th>flight</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($storage->items() as $item)
                                    <tr>
                                        <td>
                                            {{ $item->unit->name }}
                                            {{ $item->number }}
                                        </td>
                                        <td>
                                            {{ $item->code }}
                                        </td>
                                        <td>
                                            {{ $item->number }}
                                        </td>
                                        <td>
                                            {{ $item->quantity }}
                                        </td>
                                        <td>
                                            <a href="{{ route('flights.show', ['flights'=>$item->flight->id]) }}">{{ $item->flight->flight_route->flightNumber() }}</a>
                                        </td>
                                        <td>
                                            <a href="{{ route('storage.edit', ['storage'=>$item->id]) }}">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="{{ route('storage.create') }}" class="btn btn-primary">Create</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
