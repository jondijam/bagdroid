@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Unit load device</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>Serialcode</th>
                                    <th>Barcode</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($unitLoadDevices->items() as $unitLoadDevice)
                                    <tr>
                                        <td>{{ $unitLoadDevice->type }}</td>
                                        <td>{{ $unitLoadDevice->serial_code }}</td>
                                        <td>{{ $unitLoadDevice->barcode }}</td>
                                        <td>
                                            <a href="{{ route('unit_load_devices.edit',['unit_load_devices'=>$unitLoadDevice->id]) }}">Edit</a>
                                            <a href="{{ route('unit_load_devices.show', ['unit_load_devices'=>$unitLoadDevice->id]) }}">Download</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <a href="{{ route('unit_load_devices.create') }}" class="btn btn-primary">Create</a>
            </div>
        </div>
    </div>
@stop