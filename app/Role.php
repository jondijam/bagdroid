<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;
use Config;

class Role extends EntrustRole
{

    public function scopeRole($query, $role)
    {
        return $query->where('name', '=', $role);
    }

    public function teams()
    {
        return $this->belongsToMany(Config::get('teamwork.team_model'), Config::get('teamwork.team_user_table'), 'role_id','team_id')->withPivot('user_id')->withTimestamps();
    }

    public function teamUsers()
    {
        return $this->belongsToMany(Config::get('teamwork.user_model'), Config::get('teamwork.team_user_table'), 'role_id','user_id')->withPivot('team_id')->withTimestamps();
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
